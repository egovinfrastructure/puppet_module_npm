define npm::global () {
  exec { "${title}_g_install":
    command => "/usr/bin/npm install -g $title",
    creates => "/usr/bin/$title",
    require => Package["npm"],
  }
}
